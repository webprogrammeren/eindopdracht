<?PHP
session_start();
$_SESSION['sessionid'] = session_id();

if (isset($_GET['ajax'])) {
    $data = file_get_contents("game.json");
    header('Content-Type: application/json');
    print($data);
    exit();
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, width=device-width,
          initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Eindopdracht voor webprogrammeren, RUG"/>
    <title>Boter-Kaas-Eieren</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
          rel="stylesheet">

</head>
<body>
    <div class="container">
        <!-- Titel -->
        <header id="header">BOTER KAAS EN EIEREN</header>
        <!-- Buttons om aan te melden, te stoppen, of door te gaan -->
        <input type="button" value="Aanmelden" id="join">
        <input type="button" value="Stoppen" id="leave">
        <input type="button" value="Doorgaan" id="nextGame">
        <!-- Scorebord -->
        <table id="spelMeta">
            <thead>
            <tr>
                <th id="th1">Speler 1</th>
                <th id="th0"></th>
                <th id="th2">Speler 2</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td id="player1score">0</td>
                <td ></td>
                <td id="player2score">0</td>
            </tr>
            </tbody>
        </table>
        <!-- Speelvelden -->
        <table id="spel">
            <tbody>
            <tr>
                <td id="0"></td>
                <td id="1"></td>
                <td id="2"></td>
            </tr>
            <tr>
                <td id="3"></td>
                <td id="4"></td>
                <td id="5"></td>
            </tr>
            <tr>
                <td id="6"></td>
                <td id="7"></td>
                <td id="8"></td>
            </tr>
            </tbody>
        </table>
        <!-- Info -->
        <span id="infobox">
            <button id="info">info</button>
        </span>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <h3>Hoe speel je het spel?</h3><br>
                <p>Je kunt dit spel in je eentje of met twee spelen.
                    Als je het spel wil spelen, klik je eerst op de knop ‘aanmelden’ om te kunnen beginnen.</p>
                <p>Klik daarna op de vakjes in de tabel om hier een X of een O te plaatsen en probeer een rij,
                    kolom of diagonaal met driemaal hetzelfde teken te maken voordat je tegenstander dat kan.
                    Elke keer dat dit lukt, krijg je een punt.</p>
                <p> Om door te gaan met het spel,
                    kun je op de knop ‘doorgaan’ klikken.Je kunt zo vaak spelen als je wilt,
                    je score wordt gewoon bijgehouden.</p>
                <p>Als je wilt stoppen met spelen,
                    kun je op de knop ‘stoppen’ drukken, de pagina refreshen, of gewoon het tabblad sluiten.
                    <br><br>Veel spelplezier!</p>
                <span class="close">&times;</span>
            </div>
        </div>
        <!-- Communicatiebord -->
        <span id="turnBoard"></span>
        <!-- Aanmeldstatus spelers -->
        <div id="online">
            <div id="player1icon">
                <div id="figHead" class="fig1"></div>
                <div id="figTorso" class="fig1"></div>
            </div>
            <div id="player2icon">
                <div id="figHead" class="fig2"></div>
                <div id="figTorso" class="fig2"></div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/modal.js"></script>
</body>
</html>
