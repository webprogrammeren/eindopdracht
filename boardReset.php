<?php
/* Dit script wordt aangeroepen als zich een tweede speler heeft aangemeld.
Alle speelvelden worden dan leeggehaald en de scores weer op nul gezet.*/
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen

/*maakt php array om vervolgens het json bestand te updaten; daarin wordt de spelbeurt
aan speler 2 gegeven om de game flow te bevorderen en de join op 1 gezet als signaal dat
de laatste actie in het spel een aanmelding betrof */
$gameReset = array("players" => 2, "pause" => 0, "player1" => $game['player1'],
    "player2" => $game['player2'], "turn" => "player2", "player1ready" => 0,
    "player2ready"=> 0, "player1score" => 0, "player2score"=>0,
    "board" => array("", "", "", "", "", "", "", "", ""),
    "winningSquares" => array(0,0,0), "lastWinner" => 0, "numberGames" => 0,
    "join" => 1, "leave" => 0, "reset" => 0);

$output = json_encode($gameReset); //zet de array om naar een json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json
?>