<?php
/* Dit script updatet het json bestand nadat een speler op de knop 'doorgaan' heeft
geklikt zodat een nieuw spel kan beginnen. */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen
++$game['numberGames'];   //hoog het aantal games met 1 op

/*gebruikt even of oneven games om te bepalen welke speler de eerste beurt krijgt
tenzij het aantal spelers twee is, dan begint namelijk de speler die het laatst op
de doorgaan (wachten) button klikt ter bevestiging van zijn keuze om door te gaan*/

if ($game['players'] === 1){  //bij één speler
    if ($game['numberGames'] % 2 === 0) {  //indien het aantal games even is
        $turn = 'player1';      //geeft spelbeurt aan speler 1
    } else {
        $turn = 'virtualPlayer2';
    }
} else {   //bij twee spelers
    if ($game['player1'] === $id) {  //indien speler 1 als laatste op de doorgaan (wachten) knop drukte
        $turn = 'player1'; //dan mag hij gelijk verder met de eerste zet in het nieuwe spel
    } else {
        $turn = 'player2'; //indien speler 2 als laatste op diezelfde knop drukte, dan mag hij de eerste zet doen
    }
}

//maakt php array om daarmee vervolgens het json bestand te kunnen updaten
$gameReset = array("players" => $game['players'], "pause" => 0, "player1" => $game['player1'],
    "player2" => $game['player2'], "turn" => $turn, "player1ready" => 0, "player2ready"=> 0,
    "player1score" => $game['player1score'], "player2score" => $game['player2score'],
    "board" => array("", "", "", "", "", "", "", "", ""), "winningSquares" => array(0,0,0),
    "lastWinner" => 1, "nextGame" => 1, "numberGames" => $game['numberGames'],
    "join" => 0, "leave" => 0, "reset" => 0);

$output = json_encode($gameReset); //zet de array om naar json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json
?>