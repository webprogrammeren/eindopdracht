<?php
/* Dit script updatet het json bestand nadat een speler zich heeft aangemeld
middels de aanmeldknop. */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen

$game['pause'] = 0;   //zet de pauzestand bij een aanmelding steeds uit zodat meteen kan worden gespeeld
$game['join'] = 1;   //zet aanmeldstatus op 1 als signaal dat laatste actie aanmelding betreft

switch ($game['players']) {     //een switch statement met het aantal spelers als de verschillende cases
    case 0 :                    //indien aantal spelers 0 is
        if ($game['player1'] != $id) {   //om te voorkomen dat eenzelfde speler zich meermaals kan aanmelden
            $game['player1'] = $id;      //zet speler 1 in spelbestand gelijk aan het ID
            echo 0;                   //geeft signaal af aan main.js zodat daar de nodige actie kan worden ondernomen
            $game['players'] = 1;       //zet het aantal spelers in het spelbestand gelijk aan 1
            $game['turn'] = "player1";   //geeft de spelbeurt aan speler 1
        }
        break;
    case 1 :        //indien aantal spelers 1 is
        if (($game['player2'] != $id) && ($game['player1'] != $id)) {
            $game['player2'] = $id;     //zet speler 2 in het spelbestand gelijk aan het ID
            echo 1;                 //geeft signaal af aan main.js zodat daar de nodige actie kan worden ondernomen
            $game['players'] = 2;       //zet aantal spelers in het spelbestand op 2
        }
        break;
    case 2 :        //indien aantal spelers 2 is
        echo 2;     //geeft signaal af aan main.js zodat daar de nodige actie kan worden ondernomen
        break;
}

$output = json_encode($game); //zet de array om naar een json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json

?>