<?php
/* Dit script updatet het json bestand nadat een speler het spel heeft verlaten door
zich af te melden middels de stopknop, een page refresh of het sluiten van het browser window */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game);//converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen

if ($game['player1'] === $id || $game['player2'] === $id) {
    //enkel aangemelde spelers kunnen het spel verlaten
    if ($game['players'] === 1) {
        //als er 1 speler in het spel was
        $game['player1'] = "";
        $game['player2'] = "";
        --$game['players'];   //vermindert het spelersaantal met 1
    } else if ($id === $game['player1']) {
        //als er 2 spelers waren en speler 1 heeft de afmeldactie uitgevoerd
        $game['player1'] = $game['player2'];   //speler 1 wordt speler 2
        $game['player2'] = "virtualPlayer2";   //speler 2 wordt virtuele speler
        --$game['players'];   //vermindert het spelersaantal met 1
    } else if ($id === $game['player2']) {  //als speler 2 de afmelding heeft uitgevoerd
        $game['player2'] = "";
        --$game['players'];   //vermindert het spelersaantal met 1
    }

    /*maakt php array met daarin verwerkt de nieuwe values voor de spelers en het spelersaantal.
    Ook wordt leave op 1 gezet om aan te geven dat de laatste actie in het spel een afmelding was*/
    $game = array("players" => $game['players'], "pause" => $game['pause'],
        "player1" => $game['player1'], "player2" => $game['player2'], "turn" => "player1",
        "player1ready" => 0, "player2ready"=> 0, "player1score" => 0, "player2score"=> 0,
        "board" => array("", "", "", "", "", "", "", "", ""),
        "winningSquares" => array(0,0,0), "lastWinner" => 0, "numberGames" => 0,
        "join" => 0, "leave" => 1, "reset" => 0);

    $output = json_encode($game); //zet de array om naar een json string
    file_put_contents("game.json", $output);  //schrijft json weg naar game.json
}
?>