<?php
/* Dit script checkt of het next.php script kan worden uitgevoerd wanneer
op de knop 'doorgaan' werd geklikt. Het script is enkel relevant bij twee
spelers, aangezien daar bevestiging van de tweede speler nodig is bij doorspelen.
Indien de status van één van de spelers op 0 staat, zal de spelstatus op "wait" worden gezet.
Dit is het signaal voor main.js om te wachten met het uitvoeren van next.php */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game);//converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen

if ($game['player1'] === $id) { // indien speler 1 op knop drukte,
    $game['player1ready']=1;    // zet zijn status dan op 1
} else {                        // indien speler 2 op knop drukte,
    $game['player2ready']=1;    // zet zijn status dan op 1
}

if (($game['players'] === 2) && (($game['player1ready'] === 0) || ($game['player2ready'] === 0))) {
    //indien de status van één van de spelers op 0 staat, zet de spelstatus dan op 'wait'
    echo "wait";
}

$output = json_encode($game); //zet de array om naar een json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json

?>