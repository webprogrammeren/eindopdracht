<?php
/* Dit script zorgt ervoor dat op elk spelmoment de geschikte berichten
 * op de relevante schermen onderin terechtkomen.
 */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen
$turn = $game['turn'];
$boardLen = strlen(implode($game['board'])); //bepaalt lengte van het aantal gevulde speelvelden

if (($id === $game['player1']) || ($id === $game['player2'])) {
    //zorgt ervoor dat in-game spelberichten enkel gezien worden door de aangemelde speler(s)
    if ($game['winningSquares'] !== array(0, 0, 0)) {
        //als er een score is, m.a.w. als de array met winnende velden niet leeg is
        if (($game['players'] === 1)) {   //als er één speler is, print dan 'score!' op het scherm bij een score
            echo 'Score!!';
        } elseif (($game['players'] === 2)  //bij een spel met twee spelers
            && ($game['player1ready'] === $game['player2ready'])) { //als de status van beide spelers gelijk is
            if (($game['lastWinner'] == 1)) { //als de speler die zojuist heeft gewonnen, speler 1 is
                if ($id === $game['player1']) {  //indien het sessie id gelijk is aan dat van speler 1
                    echo 'Je hebt gewonnen!';  //print op het scherm van speler met vermeld id
                } else {
                    echo 'Je hebt verloren!';  //print op het andere scherm
                }
            } elseif ($game['lastWinner'] == 2) {  //als speler 2 zojuist heeft gewonnen
                if ($id === $game['player2']) {
                    echo 'Je hebt gewonnen!';    //print deze string op scherm speler 2
                } else {
                    echo 'Je hebt verloren!';    //print deze string op scherm speler 1
                }
            }
        } elseif (($game['players'] === 2) && ($game['player1ready'] === 1)
            && ($game['player2ready'] === 0)) {
            //als er twee spelers zijn en spelstatus speler 1 is 1 en die van speler 2 is 0
            if ($id === $game['player1']) {  //indien speler 1 als eerste op de knop doorgaan klikte
                echo 'We vragen de ander of die door wil!';  //print op scherm speler 1
            } else {
                echo 'De ander wil doorgaan, jij ook?';   //print op scherm speler 2
            }
        } elseif (($game['players'] === 2) && ($game['player1ready'] === 0)
            && ($game['player2ready'] === 1)) {
            //als er twee spelers zijn en spelstatus speler 1 is 0 en die van speler 2 is 1
            if ($id === $game['player2']) {   //indien speler 2 als eerste op de knop doorgaan klikte
                echo 'We vragen de ander of die door wil!';  //print op scherm speler 2
            } else {
                echo 'De ander wil doorgaan, jij ook?';  //print op scherm speler 1
            }
        }
    } elseif (($boardLen === 9) && ($game['winningSquares'] === array(0, 0, 0))) {
        //als er een gelijk spel is, m.a.w. als de totale lengte van de array met gevulde speelvelden gelijk is
        //aan 9 en de array met winnende velden nog steeds leeg is
        if ($game['player1ready'] === $game['player2ready']) {
            //als beide spelers dezelfde speelstatus hebben, m.a.w. als nog niemand op de knop doorgaan heeft geklikt
            //deze voorwaarde geldt ook voor een spel met 1 speler (daar zal de virtuele speler dezelfde status 0 hebben)
            echo 'Gelijk spel!!';  //print deze string
        } elseif (($game['players'] === 2) && ($game['player1ready'] === 1)
            && ($game['player2ready'] === 0)) {
            //als bij twee spelers de speelstatus van speler 1 op 1 staat en die van speler 2 op 0
            if ($id === $game['player1']) {    //als speler 1 als eerste op de knop 'doorgaan' klikte
                echo 'We vragen de ander of die door wil!';  //print string op scherm speler 1
            } else {
                echo 'De ander wil doorgaan, jij ook?';  //print string op scherm speler 2
            }
        } elseif (($game['players'] === 2) && ($game['player1ready'] === 0)
            && ($game['player2ready'] === 1)) {
            //als bij twee spelers de speelstatus van speler 1 op 0 staat en die van speler 2 op 1
            if ($id === $game['player2']) {      //als speler 2 als eerste op de knop 'doorgaan' klikte
                echo 'We vragen de ander of die door wil!';   //print string op scherm speler 2
            } else {
                echo 'De ander wil doorgaan, jij ook?';   //print string op scherm speler 1
            }
        }
    } else {  //als er geen score en geen gelijk spel is
        if ($game['players'] === 2) {   //als het spel twee spelers heeft
            if (($id === $game['player1']) && ($game['join'] === 0)) {
                /*als id sessie gelijk is aan die van speler 1 en zijn aanmeldstatus gelijk is aan 0
                  d.w.z. als zijn laatste actie geen aanmelding was */
                if ($turn === 'player1') {
                    $arr = array('Jij bent aan zet!', "Your Turn", "Jouw Beurt",
                        "De beurt is aan jou!", "He, jij bent!");
                    //maakt array met mogelijke spelmeldingen
                    $index = $boardLen % count($arr);
                    $turnMessage = $arr[$index];
                    /*neemt een string uit de array met enige variatie --
                      rand_array o.i.d. is niet mogelijk omdat het bord constant wordt ververst en je
                      daardoor een constant wisselende string op het scherm zou krijgen */
                    echo $turnMessage;
                } else {
                    $arr = array('De ander is aan zet', 'Even geduld :)', 'NIET jouw beurt!',
                        'De beurt is aan de ander!', 'De ander is aan de beurt!');
                    //maakt array met mogelijke spelmeldingen
                    $index = $boardLen % count($arr);
                    $turnMessage = $arr[$index];
                    //neemt een string uit de array met enige variatie
                    echo $turnMessage;
                }
            } elseif (($id === $game['player2']) && ($game['player1ready'] === 0)
                && ($game['player2ready'] === 0) && ($game['join'] === 0)) {
                if ($turn === 'player2') {
                    $arr = array('Jij bent aan zet!', "Your Turn", "Jouw Beurt",
                        "De beurt is aan jou!", "He, jij bent!");
                    //maakt array met mogelijke spelmeldingen
                    $index = $boardLen % count($arr);
                    $turnMessage = $arr[$index];
                    //neemt een string uit de array met enige variatie
                    echo $turnMessage;
                } else {
                    $arr = array('De ander is aan zet', 'De beurt is aan de ander!',
                        'Even geduld :)', 'De ander is aan de beurt!', 'NIET jouw beurt!');
                    //maakt array met mogelijke spelmeldingen
                    $index = $boardLen % count($arr);
                    $turnMessage = $arr[$index];
                    //neemt een string uit de array met enige variatie
                    echo $turnMessage;
                }
            } elseif ($game['join'] === 1) {
                //als de laatste actie van één van de spelers een aanmelding was
                if ($id === $game['player2']) {
                    //de speler die erbij komt is altijd speler 2; deze check is om te kunnen printen op het juiste scherm
                    echo 'Welkom, je speelt met O!';  //print op het scherm van speler 2
                } else {
                    echo 'Er is een tweede speler! Jij speelt met X!';  //print op het scherm van speler 1
                }
            }
        } elseif ($game['players'] === 1) {  //als het spel één speler heeft
            if ($game['join'] === 1) {  //als de laatste actie van de speler een aanmelding was
                echo 'Welkom!! X is aan zet';   //print welkomstboodschap
            } elseif (($game['pause'] === 0) && ($game['leave'] === 0)) {
                //als het spel gaande is zonder dat er iets bijzonders gebeurt
                if ($turn === 'player1') {
                    echo 'X is aan zet';
                    /*speler 1 speelt steeds met X - voor een spel met 1 speler wordt niet over speler 1 of 2
                      gesproken maar over X en O omdat een speler zowel echt alleen kan spelen of er twee
                      spelers kunnen zijn op dezelfde computer */
                } else {
                    echo 'O is aan zet';
                }
            } elseif ($game['leave'] === 1) {  //als de speler achtergebleven is nadat tweede speler is gestopt
                echo 'Speler 2 is gestopt!';
            } else {
                echo 'Wil je doorgaan?';  //bij meerdere kliks van de speler op speelveld in resterende situatie
            }
        }
    }
} else {
    if ($game['players'] === 0) {
        echo 'Meld je aan om te spelen!'; //bericht indien niet aangemeld en er verder geen spelers actief zijn
    } else {
        echo 'Meld je aan en speel mee!'; //bericht indien niet aangemeld en er 1 speler actief is
    }
}
?>