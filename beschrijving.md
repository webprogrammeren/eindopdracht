### Beschrijving game ###


Om het spel te kunnen spelen is het allereerst noodzakelijk dat een speler zich aanmeldt middels de aanmeldknop. Dit voorkomt dat andere webgebruikers het spel kunnen beïnvloeden en verbetert de spelervaring doordat op deze manier meer interactiviteit mogelijk is. Eenmaal aangemeld kan een speler zich op elk moment weer afmelden en het spel verlaten. Andersom kan een speler zich op elk moment ook weer aanmelden indien er op dat moment nog plek is. Het maximaal aantal aangemelde spelers is immers twee.

In de game gaat het erom dat een speler zo snel mogelijk een rij, kolom, of diagonaal met driemaal X of O bereikt. Hiermee kan een speler scoren en zijn totale score op het scorebord met één punt ophogen. De speler kan door te klikken op één van de speelvelden bepalen wat erop verschijnt. Wat er precies verschijnen gaat, een X of O, wordt bij een spel met één speler, of twee spelers op dezelfde computer, aangegeven middels spelberichten onder op het scherm. Bij een spel met twee spelers op verschillende computers, wordt het teken X toegekend aan speler 1 en O aan speler 2.

Bij een score of gelijk spel, verschijnt er een knop waarmee de speler kan aangeven of hij wil doorgaan met een nieuw spel. Is er slechts één aangemelde speler actief, dan begint na een klik op ‘doorgaan’ meteen een nieuw spel. In een spel met twee aangemelde spelers, wordt eerst gewacht met de start van een nieuw spel totdat de andere speler het verzoek om door te spelen bevestigd heeft door op dezelfde knop ‘doorgaan’ te klikken. De speler die de bevestiging geeft, mag dan ook meteen beginnen met een eerste zet.

Indien in een spel met twee spelers er één het spel verlaat door op de knop ‘stoppen’ te klikken of door het browservenster te sluiten of te refreshen, dan krijgt de andere speler hier melding van en kan hij gewoon doorgaan met het spel. Het spelbord en de scores worden dan automatisch gereset. Bij een score, gelijk spel of lopend doorspeelverzoek, dient de achtergebleven speler wel eerst zijn keuze om door te spelen nog te bevestigen via de knop ‘doorgaan’. 

Als er één speler actief is en een ander besluit om zich aan te melden en mee te spelen, dan krijgt de eerste speler hier ook melding van. De scores en het spelbord worden dan gereset en de tweede speler mag meteen beginnen met een eerste zet.
