<?php
/* Dit script zorgt ervoor dat de reload bij een harde reset niet in een
oneindige loop terechtkomt.*/
session_start();

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen

if ($game["reset"] === 0){  //indien reset value op 0 staat, zet hem dan op 1, anders op 0
    $game["reset"] = 1;
} else {
    $game["reset"] = 0;
}

$output = json_encode($game); //zet de array om naar een json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json
?>
