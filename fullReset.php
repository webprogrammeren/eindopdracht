<?php
/* Dit script zorgt ervoor dat als alles om wat voor reden ook vastloopt
(bv. een speler die maar niet wil uitloggen en daardoor het spel blokkeert), er toch
iets van een harde reset mogelijk is zonder dat daarvoor de json opnieuw moet worden geupload.
De trigger voor dit script is als een soort 'easter egg' verborgen: het wordt in main.js getriggerd
als geklikt wordt op het div element dat de aanmeldstatus van de spelers toont */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen

//maakt php array waarin alle values worden gereset
$game = array("players" => 0, "pause" => 0, "player1" => "",
    "player2" => "", "turn" => "player1", "player1ready" => 0,
    "player2ready"=> 0, "player1score" => 0, "player2score"=> 0,
    "board" => array("", "", "", "", "", "", "", "", ""),
    "winningSquares" => array(0,0,0), "lastWinner" => 0, "numberGames" => 0,
    "join" => 0, "leave" => 0, "reset" => 0);

$output = json_encode($game); //zet de array om naar een json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json
?>



