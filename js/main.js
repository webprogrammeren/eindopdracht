$(function(){
    //verversing van het speelveld
    function fetchBoard(){
        $.ajax("index.php?ajax=true")
            .done(function(data){
                if (data.reset === 1){
                    //ververs de knoppen bij een reset met een eenmalige loop
                    $('#leave').hide();
                    $('#nextGame').hide();
                    $('#join').show();
                    $.ajax({method:"POST", url:"stopReload.php", async: false}).done(function(){
                        $.ajax({method: "POST", url: "fullReset.php", async: false}).done(function(){
                            location.reload(true);
                        });
                    });
                } else {
                    //Ververs het bord en de scores voor beide spelers (zie interval)
                    let arr = data.board;
                    for (let i = 0; i <= 8; i++) {
                        $('#' + i).empty().text(arr[i]);
                    }
                    $('#player1score').empty().text(data.player1score);
                    $('#player2score').empty().text(data.player2score);
                    let ar = data.winningSquares;
                    let winner = data.lastWinner;
                    // winnaar melding, kleur en knoppen
                    if (!((ar[0] === 0) && (ar[1] === 0))) {
                        if (winner === 1) {
                            $('#th1').css("color", "purple");
                            $('#player1score').css({
                                "color": "purple", "font-weight": "bold",
                                "border-radius": "50px", "background-color": "rgba(214, 200, 238, 0.5)"
                            });
                        } else {
                            $('#th2').css("color", "purple");
                            $('#player2score').css({
                                "color": "purple", "font-weight": "bold",
                                "border-radius": "50px", "background-color": "rgba(214, 200, 238, 0.5)"
                            });
                        }
                        if (data.players === 2) {
                            $('#nextGame').fadeIn('slow');
                        }
                        for (let i = 0; i <= 2; i++) {
                            $('#' + ar[i]).addClass("winning");
                        }
                    } else if ((data.players === 2) && (arr.toString().length === 17)) {
                        //bij een vol speelveld, laat de doorgaan knop zien.
                        $('#nextGame').fadeIn('slow');
                    } else {
                        //herstel de kleuren na een winnend veld
                        $('#player1score').css({
                            "color": "black",
                            "font-weight": "normal", "background-color": "#D7F5F5"
                        });
                        $('#player2score').css({
                            "color": "black",
                            "font-weight": "normal", "background-color": "#D7F5F5"
                        });
                        $('#th1').css("color", "black");
                        $('#th2').css("color", "black");
                        if (data.players === 2) {
                            $('#nextGame').fadeOut('slow').val("Doorgaan");
                        }
                        for (let i = 0; i <= 8; i++) {
                            $('#' + i).removeClass("winning");
                        }
                    }
                    //pas de tekst in het score tabelletje aan op basis van aantal spelers
                    if ((data.players === 2) || (data.players === 0)) {
                        $('#th1').empty().text("Speler 1");
                        $('#th2').empty().text("Speler 2");
                    } else {
                        $('#th1').empty().text("Score X");
                        $('#th2').empty().text("Score O");
                    }

                    // Alle meldingen op het turnboard, gedefinieerd in turnboard.php
                    $.ajax({method: "POST", url: "turnBoard.php"})
                        .done(function (data) {
                            $('#turnBoard').text(data);
                        });
                    // De visuele player icons van de juiste kleuren voorzien (obv aantal spelers, 0,1 of 2).
                    // Kleuren gedefinieerd in css
                    if (data.players === 1) {
                        $('#nextGame').val("Doorgaan");
                        if (data.turn === 'player1') {
                            $('.fig1').removeClass('gradGray gradBlue').addClass('gradGreen');
                            $('.fig2').addClass('gradGray');
                        } else {
                            $('.fig1').removeClass('gradGray gradGreen').addClass('gradBlue');
                            $('.fig2').addClass('gradGray');
                        }
                    } else if (data.players === 2) {
                        if (data.turn === 'player1') {
                            $('.fig1').removeClass('gradGray gradBlue').addClass('gradGreen');
                            $('.fig2').removeClass('gradGray gradGreen').addClass('gradBlue');
                        } else {
                            $('.fig2').removeClass('gradGray gradBlue').addClass('gradGreen');
                            $('.fig1').removeClass('gradGray gradGreen').addClass('gradBlue');
                        }
                    } else if (data.players === 0) {
                        $('.fig1').addClass('gradGray');
                        $('.fig2').addClass('gradGray');
                    }
                }
            });
    }

    $('#join').on('click', function(){
        //Na klikken op het join event, registreer de speler (wanneer mogelijk) en pas de knoppen aan.
        $.ajax({method: "POST", url: "join.php"})
            .done(function(data){
                let players = Number(data);
                if (players === 0){
                    alert('Je bent speler 1');
                    $('#join').hide();
                    $('#info').hide();
                    $('#leave').show();
                } else if (players === 1){
                    alert('Je bent speler 2');
                    $.ajax({method:"POST", url:"boardReset.php"});
                    $('#nextGame').fadeOut('slow');
                    $('#join').hide();
                    $('#info').hide();
                    $('#leave').show();
                } else if (players === 2) {
                    alert('Er zijn al twee spelers. Probeer het later nog eens!');
                }
                fetchBoard();
            });
    });

    $('#nextGame').on('click', function() {
        //Na klikken op doorgaan (zichtbaar bij vol veld of winnend veld, wacht tot beide spelers
        //door willen en pas de knoppen aan.
        $.ajax({method: "POST", url: "ready.php"})
            .done(function(data){
                if (data === "wait") {
                    $('#nextGame').val("Wachten");
                } else {
                    $('#nextGame').val("Doorgaan");
                    $('#nextGame').fadeOut('slow');
                    $.ajax({method: "POST", url: "next.php"})
                        .done(function(data){
                            fetchBoard();
                        });
                    }
            });
    });

    $('#leave').on('click', function(){
        //Na klikken op stoppen wordt de speler correct uitgeschreven, het spel vernieuwd en kan er
        //verder gespeeld worden. Past de knoppen en tekst weer aan.
        $.ajax({method:"POST", url:"leave.php"})
            .done(function(){
                $('#leave').hide();
                $('#nextGame').hide();
                $('#join').show();
                $('#info').show();
                $('#th1').empty().text("Score X");
                $('#th2').empty().text("Score O");
                fetchBoard();
            });
    });

    $('#spel').on('click', function(e){
        //Na klikken op het speeldveld, registreer de target (veld0-8), check speler
        // en veldinhoud in php en pas het veld aan in json
        let target = $(e.target).attr('id');
        let targetContent = document.getElementById(target).innerText;
        $.ajax({method:"POST", url:"game.php", data:{target: target}})
            .done(function(data){
                fetchBoard();
                //waarschuwingen bij klikactie op veld
                if (data === "OthersTurn"){
                    alert("Niet jouw beurt!");
                } else if (data ==="RegisterNow") {
                    alert('Meld je eerst aan!');
                } else if (data ==="player1") {
                    $('#nextGame').fadeIn('slow');
                } else if (data ==="player2") {
                    $('#nextGame').fadeIn('slow');
                }
            });
    });

    $('#online').on('click', function(){
        //verborgen knop om het spel in orginele staat te zetten voor demo
        //aangezien nieuwe spel instances niet mogelijk zijn
        $.ajax({method: "POST", url: "stopReload.php"})
            .done(function(data){
                fetchBoard();
            });
    });

    $(window).on('beforeunload', function(){
        //registreer wanneer iemand de pagina verlaat, zodat deze speler wordt uitgeschreven
        $.ajax({method:"POST", url:"leave.php", async: false})
            .done(function(data){
                fetchBoard();
            });
    });

    fetchBoard();
    // Ververs het speelveld elke 1000 ms
    setInterval(fetchBoard, 1000);
});