<?php
/* Dit script houdt de algemene spelvoortgang bij en schrijft bij elke zet de spelstand weg naar het json bestand. */
session_start();
$id = $_SESSION['sessionid'];

$data = file_get_contents("game.json"); //leest json file in als string
$game = json_decode($data);  //maakt van ingelezen json een array
$game = get_object_vars($game); //converteert de objecten in de opgehaalde array
$target = htmlspecialchars($_POST['target']); //krijgt het id van het aangeklikte speelveld
$board = $game['board'];
$index = (int)($target); //converteert de objecten in de opgehaalde array zodat we een associatieve php array krijgen
$targetContent = strval($board[$index]); //krijgt de content van het aangeklikte speelveld

if (($game['player1'] == $id) || ($game['player2'] == $id)){
    //deze voorwaarde maakt dat enkel aangemelde spelers kunnen spelen
    if ((strlen($targetContent) == 0)) {   //als aangeklikt speelveld leeg is
        if (($game['players'] === 1) && ($game['pause'] == 0)) {
            //als er één speler is en de game niet op pauzestand staat
            if ($game['turn'] == "player1") {
                $game['board'][$index] = "X";   //zet 'X' op het speelveld
                $game['turn'] = "virtualPlayer2";  // geeft de beurt aan virtuele speler 2
                /*als er 1 speler is werkt het spel met een virtuele tweede speler - virtueel in die zin
                dat het sessie ID van die speler niet bekend is omdat hij niet is aangemeld */
            } else if ($game['turn'] == "virtualPlayer2") {
                $game['board'][$index] = "O";   //zet 'O' op het speelveld
                $game['turn'] = "player1";   //geeft de beurt weer aan speler 1
            }
        }
        if (($game['players'] === 2) && ($game['pause'] == 0)) {
            //als het aantal spelers twee is en de game niet op pauzestand staat
            if ($game['turn'] == "player1") {  //als speler 1 aan de beurt is
                if ($game['player1'] === $id) { //als sessie ID van degene die klikt overeenkomt met dat van speler 1
                    $game['board'][$index] = "X";    //zet een 'X' op het desbetreffende speelveld
                    $game['turn'] = "player2";        //geeft de beurt weer aan speler 2
                } else {
                    echo "OthersTurn";
                    //als speler 2 op een speelveld klikt wanneer speler 1 aan de beurt is, krijgt main.js dit signaal door
                }
            } else if ($game['turn'] == "player2") {
                if ($game['player2'] === $id) {
                    $game['board'][$index] = "O";
                    $game['turn'] = "player1";
                } else {
                    echo "OthersTurn";
                    //als speler 1 op een speelveld klikt wanneer speler 2 aan de beurt is, krijgt main.js dit signaal door
                }
            }
        }

        $board = $game['board'];
        $success = 0;    //deze variabele wordt gebruikt om bij te houden of er gescoord is
        $winPositions = array(array(0, 1, 2), array(3, 4, 5), array(6, 7, 8),
            array(0, 3, 6), array(1, 4, 7), array(2, 5, 8), array(0,4, 8), array(2, 4, 6));
        //dit is een array met als subarrays alle mogelijke speelveldcombinaties die winnend zijn
        foreach ($winPositions as $subArray) {
            /*loopt over alle mogelijke winnende speelveldcombinaties en kijkt of de string
              die gevormd wordt door de huidige content op die winnende speelveldreeksen overeenkomt
              met een winnende string XXX of OOO*/
            if ($board[$subArray[0]] . $board[$subArray[1]] . $board[$subArray[2]] === "XXX") {
                //als één van de winnende speelveldcombinaties de string XXX oplevert
                $game['lastWinner'] = 1;    //update spelbestand met als winnaar speler 1, want die speelt steeds met X
                if ($game['pause'] != 1){
                    ++$game['player1score'];
                }  //de extra conditie voorkomt dat de speler zijn score kan ophogen terwijl de game op pauzedtand staat
                $game['pause'] = 1;         //zet game op pauzestand
                $game['winningSquares'] = $subArray;   //update het spelbestand met de winnende subarray
                $success = 1;
                echo "player1";   //geeft winnaar door aan main.js
                break;
            } else if ($board[$subArray[0]] . $board[$subArray[1]] . $board[$subArray[2]] === "OOO") {
                //als één van de winnende speelveldcombinaties de string OOO oplevert
                $game['lastWinner'] = 2;    //update spelbestand met als winnaar speler 2, want die speelt steeds met O
                if ($game['pause'] != 1) {
                    ++$game['player2score'];
                }  //de extra conditie voorkomt dat de speler zijn score kan ophogen terwijl de game op pauzestand staat
                $game['pause'] = 1;         //zet game op pauzestand
                $game['winningSquares'] = $subArray;  //update het spelbestand met de winnende subarray
                $success = 1;
                echo "player2";   //geeft winnaar door aan main.js
                break;
            }
        }
        if (($success === 0) && (strlen(implode($game['board'])) === 9)){
            /*als er een gelijk spel is, d.w.z. als er geen winnaar is en het bord is wel helemaal gevuld:
            Het eerste wordt gecheckt door de variabele $success die op 0 blijft staan als er geen winnende
            velden gevonden zijn. Het tweede wordt gecheckt door te kijken of de totale lengte van gevulde
            speelvelden gelijk is aan 9*/
            echo "player1";
            $game['pause'] = 1;   //zet game op pauzestand
        }
    }
} else {
    echo "RegisterNow";  /*indien men klikt op het spelbord maar niet is aangemeld, wordt er een signaal
                           naar main.js gezonden dat aanmelden noodzakelijk is*/
}
$game['join'] = 0;      //zet signaal van aanmelding terug op nul zodra spelers gaan spelen
$game['leave'] = 0;     //zet signaal van een afmelding terug op nul zodra speler weer een zet doet
$output = json_encode($game); //zet de array om naar een json string
file_put_contents("game.json", $output);  //schrijft json weg naar game.json
?>